<?php

namespace Tests\Unit;

use App\Services\ClientService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ValidateIfIsAFraudsterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function validatesIfIsAFraudster()
    {
        $this->withoutExceptionHandling();
        $request = [
            "name"=> "Flor Rebeca Pacheco",
            "birthday"=> "1977-06-09",
            "rg"=> "965116026",
            "cpf"=> "94876212104",
            "avatar"=> "avatar.ppg",
            "postal_code"=> "65065470",
            "public_place"=> "R. Cortês",
            "number"=> "7373",
            "district"=> null,
            "city"=> "Vila Priscila do Sul",
            "state"=> "AP"
        ];

        $this->post(route('clients.store'), $request)->assertCreated();

        $result = (new ClientService())->searchClient(['cpf' => $request['cpf']]);

        $this->assertEquals("{$request['cpf']} é um possível fraudador!", $result->getData());
    }
}
