<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Faker\Factory;
use Faker\Provider\pt_BR\Address;

class ClientsControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function get_all_clients ()
    {
        $this->get(route('clients.index'))
            ->assertOk();
    }

    /** @test */
    public function can_create_client()
    {
        $this->withoutExceptionHandling();
        Storage::fake('avatars');

        $request = [
            "name"=> "Flor Rebeca Pacheco",
			"birthday"=> "1977-06-09",
			"rg"=> "965116026",
			"cpf"=> "94876212104",
			"avatar"=> "avatar.ppg",
			"postal_code"=> "65065470",
			"public_place"=> "R. Cortês",
			"number"=> "7373",
			"district"=> null,
			"city"=> "Vila Priscila do Sul",
			"state"=> "AP"
        ];

        $this->post(route('clients.store'), $request)->assertCreated();

        $this->assertDatabaseHas('clients', [
            'name' => $request['name'],
            'birthday' => $request['birthday'],
            'rg' => $request['rg'],
            'cpf' => $request['cpf'],
            'avatar' => $request['avatar'],
            "postal_code" => $request['postal_code'],
            'public_place' => $request['public_place'],
            'number' => $request['number'],
            'city' => $request['city'],
            'state' => $request['state'],
        ]);
    }

    /** @test */
    public function it_validate_required_rules()
    {
        $request = [
            'name' => '',
            'birthday' => '',
            'rg' => '',
            'cpf' => '',
            'avatar' => '',
            "postal_code"=> "",
            'public_place' => '',
            'number' => '',
            'city' => '',
            'state' => ''
        ];

        $this->post(route('clients.store'), $request)
            ->assertStatus(422);
    }

    /** @test */
    public function it_validate_unique_rules()
    {
        $faker = Factory::create('pt_BR');

        $request = [
            'name' => $this->faker->name,
            'birthday' => $faker->date(),
            'rg' => $faker->rg(false),
            'cpf' => $faker->cpf(false),
            'avatar' =>  UploadedFile::fake()->image('avatar.jpg'),
            "postal_code"=> "65065470",
            'public_place' => $faker->streetName(),
            'number' => $faker->randomNumber(),
            'city' => $faker->city(),
            'state' => Address::stateAbbr()
        ];

        $this->post(route('clients.store'), $request);
        $this->post(route('clients.store'), $request)
            ->assertStatus(422);
    }

    /** @test */
    public function it_validate_name_min_rules()
    {
        $faker = Factory::create('pt_BR');

        $request = [
            'name' => 'Gil',
            'birthday' => $faker->date(),
            'rg' => $faker->rg(false),
            'cpf' => $faker->cpf(false),
            'avatar' =>  UploadedFile::fake()->image('avatar.jpg'),
            "postal_code"=> "65065470",
            'public_place' => $faker->streetName(),
            'number' => $faker->randomNumber(),
            'city' => $faker->city(),
            'state' => Address::stateAbbr()
        ];

        $this->post(route('clients.store'), $request)
            ->assertStatus(422);
    }

    /** @test */
    public function it_check_client_search()
    {
        $faker = Factory::create('pt_BR');

        $client = [
            'name' => $this->faker->name,
            'birthday' => $faker->date(),
            'rg' => $faker->rg(false),
            'cpf' => $faker->cpf(false),
            'avatar' =>  UploadedFile::fake()->image('avatar.jpg'),
            "postal_code"=> "65065470",
            'public_place' => $faker->streetName(),
            'number' => $faker->randomNumber(),
            'city' => $faker->city(),
            'state' => Address::stateAbbr()
        ];

        $this->post(route('clients.store'), $client);

        $params = ['name' => $client['name']];

        $this->post(route('clients.search'), $params)
            ->assertSeeText($client['name']);

        $this->assertEquals($client['name'], $params['name']);
    }
}
