<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'min:4', Rule::unique('clients', 'name')->ignore($this->client)],
            'birthday' => ['required'],
            'rg' => ['required'],
            'cpf' => ['required'],
            'postal_code' => ['required'],
            'public_place' => ['required'],
            'number' => ['required'],
            'city' => ['required'],
            'state' => ['required', 'max:2']
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'required'  => ':attribute é obrigatório!',
            'unique'    => 'Já existe um registro para esse :attribute!',
            'min'       => ':attribute não pode ser menor que :min!',
            'max'       => ':attribute não pode ser maior que :max!',
        ];
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        return [
            'name' => 'Nome',
            'birthday' => 'Data de Nascimento',
            'rg' => 'RG',
            'cpf' => 'CPF',
            'avatar' => 'Foto de perfil',
            'public_place' => 'Logradouro',
            'number' => 'Número',
            'city' => 'Município',
            'state' => 'UF'
        ];
    }

//    /**
//     * @return void
//     */
//    public function prepareForValidation() :void
//    {
//        $this->merge([
//            'rg' => preg_replace('/\D/', '', $this->rg),
//            'cpf' => preg_replace('/\D/', '', $this->cpf)
//        ]);
//    }
}
