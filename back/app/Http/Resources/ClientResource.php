<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * @param Request $request
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'birthday' => $this->birthday,
            'rg' => $this->rg,
            'cpf' => $this->cpf,
            'avatar' => $this->avatar,
            'public_place' => $this->public_place,
            'number' => $this->number,
            'district' => $this->district,
            'city' => $this->city,
            'state' => $this->state
        ];
    }
}
