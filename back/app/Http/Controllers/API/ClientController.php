<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Models\Client;
use App\Services\Contracts\ClientServiceInterface;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ClientController extends Controller
{
    /**
     * @param ClientServiceInterface $clientService
     */
    public function __construct(protected ClientServiceInterface $clientService) {}

    /**
     * @return mixed
     */
    public function index(): mixed
    {
        $clients = Client::all();
        return $this->clientService->getAll($clients);
    }

    /**
     * @return mixed
     */
    public function search(): mixed
    {
        return $this->clientService->searchClient(request()->only(['name', 'cpf', 'rg']));
    }

    /**
     * @param  ClientRequest  $request
     * @return JsonResponse
     */
    public function store(ClientRequest $request): JsonResponse
    {
        try {
            $validator = validator()->make($request->all(), $request->rules(), $request->messages());

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors(),
                    'message' => 'Ocorreu um erro! Por favor, tente novamente.'
                ]);
            }

            $data = $this->clientService->saveData($request->validated());

            return response()->json($data, ResponseAlias::HTTP_CREATED);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage());
        }
    }
}
