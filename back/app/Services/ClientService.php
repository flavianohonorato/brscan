<?php

namespace App\Services;

use App\Http\Resources\ClientResource;
use App\Models\Client;
use App\Services\Contracts\ClientServiceInterface;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ClientService implements ClientServiceInterface
{
    public function getAll($clients) :AnonymousResourceCollection
    {
        return ClientResource::collection($clients);
    }

    /**
     * @param  array  $request
     * @return Client
     */
    public function saveData(array $request) :Client
    {
        return Client::create($request);
    }

    /**
     * @param  array  $params
     * @return mixed
     * @throws Exception
     */
    public function searchClient(array $params) :mixed
    {
        if (isset($params['cpf']) && Client::validatesIfIsAFraudster($params['cpf'])) {
            return response()->json("{$params['cpf']} é um possível fraudador!", 404);
        }

        return Client::query()
            ->filter($params)
            ->get();
    }
}
