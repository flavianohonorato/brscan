<?php

namespace App\Services\Contracts;

use App\Models\Client;

interface ClientServiceInterface
{
    public function getAll($clients);

    public function saveData(array $request) :Client;

    public function searchClient(array $params) :mixed;
}
