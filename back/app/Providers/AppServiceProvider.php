<?php

namespace App\Providers;

use App\Services\ClientService;
use App\Services\Contracts\ClientServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        app()->bind(ClientServiceInterface::class, ClientService::class);
    }
}
