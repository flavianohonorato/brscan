<?php

namespace App\Models;

use App\Scopes\FilterTrait;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory, FilterTrait;

    const UNTIL_1950 = [0, 1, 2, 3];
    const UNTIL_2000 = [4, 5, 6];
    const SINCE_2001 = [7, 8, 9];

    /**
     * @var string
     */
    protected $table = 'clients';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'birthday',
        'rg',
        'cpf',
        'avatar',
        'postal_code',
        'public_place',
        'neighbordhood',
        'number',
        'district',
        'city',
        'state'
    ];

    /**
     * @var array
     */
    protected array $likeFilterFields = ['name', 'cpf', 'rg'];

    /**
     * @var array
     */
    protected array $boolFilterFields = [];

    /**
     * @param $cpf
     * @return bool
     * @throws Exception
     */
    public static function validatesIfIsAFraudster($cpf): bool
    {
        if (!self::checkIfExistsByField('cpf', $cpf)) {
            throw new Exception('CPF nao encontrado!');
        }

        $firstPosition = substr($cpf, 0, 1);

        if (in_array($firstPosition, self::UNTIL_1950)) {
            return false;
        }

        if (in_array($firstPosition, self::UNTIL_2000)) {
            return false;
        }

        return true;
    }

    /**
     * @param $field
     * @param $value
     * @return bool
     */
    public static function checkIfExistsByField($field, $value) :bool
    {
        return (bool) static::where($field, $value)->first();
    }
}
