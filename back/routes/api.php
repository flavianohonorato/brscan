<?php

use App\Http\Controllers\API\ClientController;
use Illuminate\Support\Facades\Route;

Route::middleware('api-response')->group(function () {
    Route::get('clients/search', [ClientController::class, 'search'])->name('clients.search');
    Route::apiResource('clients', ClientController::class);
});
