
## BrScan

Foi desenvolvido uma aplicação de clientes, com objetivo de verificar se os clientes cadastrados podem ser potenciais fraudadores, com base
em regra préestabelecida pela BrScan.

Para esta aplicação foi utilizado Laravel, como API, e VueJs para a parte do frontend.
Foi utilizado docker, ao rodar o comando `docker-compose up`, os containers irão subir, e os seguintes endpoints estação disponíveis:
### API http://localhost:9001
### FRONT http://localhost:9002

Apesar de que foi utilizado docker, é possível subir as aplicações, no caso do laravel, pode-se usar o comando: `php artisan serve`, e no
front end pode-se utilizar o comando `npm run dev`


### Endpoints

## Listagem de Clients

**URL** : `/api/clients`

**Method** : `GET`

## Success Response

**Code** : `200 OK`

**Content examples**
```json
{
  "data": [
    {
      "id": 1,
      "name": "Flor Rebeca Pacheco",
      "birthday": "1977-06-09",
      "rg": "965116026",
      "cpf": "94876212104",
      "avatar": null,
      "public_place": "R. Cortês",
      "number": "7373",
      "district": null,
      "city": "Vila Priscila do Sul",
      "state": "AP"
    },
    ...
  ]
}
```

# Busca por Cliente

Neste endpoint é possível fazer buscas por clientes, seja por Nome, CPF ou RG 

**URL** : `/api/clients/search`

**Method** : `GET`

**Data constraints**

**Examplo** É possível enviar somente um campo, ou todos(nome, cpf, rg).

```json
{
    "name": "Fulano de tal",
    "cpf": "39018769010",
    "rg": "324939206"
}
```

#### Se encontrar algo na busca, o retorno é similar ao de listagem


# Salvar novo Cliente

Para criação de um cliente, é preciso satisfazer todas as validações

**URL** : `/api/clients/`

**Method** : `POST`

**Exemplo**.

```json
{
      "name": "Jane Doe",
      "birthday": "03/06/1985",
      "rg": "283068724",
      "cpf": "98769558028",
      "avatar": "avatar.ppg",
      "postal_code": "65606316",
      "public_place": "1ª Travessa Bela Vista",
      "number": 34,
      "city": "Caxias",
      "state": "MA"
}
```

## Success Response

**Code** : `201 CREATED`

**Content example**

```json
{
      "name": "Jane Doe",
      "birthday": "03\/06\/1985",
      "rg": "283068724",
      "cpf": "98769558028",
      "postal_code": "65606316",
      "public_place": "1ª Travessa Bela Vista",
      "number": 34,
      "city": "Caxias",
      "state": "MA",
      "updated_at": "2022-08-15T18:46:45.000000Z",
      "created_at": "2022-08-15T18:46:45.000000Z",
      "id": 4
}
```

## Error Responses(Exemplo de erros de validação)

**Condition** : Se não for informado o camp nome.

**Code** : `422`

**Headers** : `Location: /api/clients/`

**Code** : `422 BAD REQUEST`

**Content example**

```json
{
    "name": [
        "Nome é obrigatório."
    ]
}
```


## Notes

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
