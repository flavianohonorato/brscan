import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'clients',
      component: () => import('../views/Clients/Index.vue')
    },
    {
      path: '/clients/create',
      name: 'clients.create',
      component: () => import('../views/Clients/Create.vue')
    },
  ]
})

export default router
