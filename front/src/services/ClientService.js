import api from '../utils/http'

class ClientService {
    async getAll() {
        return await api.get('clients')
    }
    async search(params) {
        return await api.get(`clients/search?${params}`)
    }
    async saveClient(data) {
        return await api.post('clients', data)
    }
}

export default new ClientService()
